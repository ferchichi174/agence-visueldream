��          D      l       �   
   �   �   �   
        !  N  6  
   �  |   �  
                                  Tidio Chat Tidio Live Chat - live chat boosted with chatbots for your online business. Integrates with your website in less than 20 seconds. Tidio Ltd. http://www.tidio.com PO-Revision-Date: 2021-10-06 11:30:33+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: nl
Project-Id-Version: Plugins - Tidio – Live Chat, Chatbots &amp; Email Integration - Stable (latest release)
 Tidio Chat Tidio Live Chat - live chat versterkt met chatbots voor je online bedrijf. Integreert met je site in minder dan 20 seconden. Tidio Ltd. http://www.tidio.com 